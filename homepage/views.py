from django.shortcuts import render

# Create your views here.
def base(request):
    return render(request, 'base.html')

def newpage(request):
    return render(request, 'newpage.html')

def page(request):
    return render(request, 'page.html')
